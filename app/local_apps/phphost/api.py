import settings
import os

def sudo(cmd):
    cmd = "sudo %s"%(cmd)
    print cmd
    os.system(cmd)


def set_ftp_passwd(domain, passwd):
    sudo("ssm passwd %s %s"%(domain, passwd))

def set_mysql_passwd(domain, passwd):
    sudo("ssm passwdmysql %s %s"%(domain, passwd))

def add_hosting(domain):
    sudo("phphost-add %s &"%domain)
    sudo("phphost-addmysql %s defaultpasswd &"%domain)
