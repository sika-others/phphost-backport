from django.contrib import admin
from models import UserProfile, Hosting


admin.site.register(UserProfile)
admin.site.register(Hosting)