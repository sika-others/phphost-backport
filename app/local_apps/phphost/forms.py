from django import forms

from models import Hosting


class HostingForm(forms.ModelForm):
    class Meta:
        model = Hosting
        fields = ["domain", "hosting_type"]