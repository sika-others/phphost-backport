from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template, redirect_to

from views import *

urlpatterns = patterns('',
    url(r'^$',
        direct_to_template,
        {"template": "phphost/index.html"}, 
        name="phphost.dashboard.home", ),
    url(r'^dashboard/$',
        dashboard_home_view,
        name="phphost.dashboard.home", ),
    url(r'^dashboard/add/$',
        dashboard_hosting_add_view,
        name="phphost.dashboard.hosting.add", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/$',
        dashboard_hosting_view,
        name="phphost.dashboard.hosting", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/set-ftp-passwd/$',
        set_ftp_passwd_process,
        name="phphost.dashboard.hosting.set_ftp_passwd", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/set-mysql-passwd/$',
        set_mysql_passwd_process,
        name="phphost.dashboard.hosting.set_mysql_passwd", ),
)