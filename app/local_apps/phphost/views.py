from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required

from models import UserProfile, Hosting
from forms import HostingForm

from api import set_ftp_passwd, set_mysql_passwd, add_hosting

@login_required
def dashboard_home_view(request, template="phphost/dashboard/home.html"):
    return render_to_response(template,
        {},
        context_instance=RequestContext(request))

@login_required
def dashboard_hosting_view(request, domain, template="phphost/dashboard/hosting.html"):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: alert = request.GET["alert"]
    except KeyError: alert = ""
    return render_to_response(template,
        {"hosting": hosting, "alert": alert},
        context_instance=RequestContext(request))

def dashboard_hosting_add_view(request, template="phphost/dashboard/add_hosting.html"):
    form = HostingForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.owner = request.user.userprofile
        obj.save()
        add_hosting(obj.domain)
        return HttpResponseRedirect((reverse("phphost.dashboard.hosting", args=[obj.domain, ])))
    return render_to_response(template,
        {"form": form},
        context_instance=RequestContext(request))

@login_required
def set_ftp_passwd_process(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: passwd = request.POST["passwd"]
    except KeyError: passwd = None
    if passwd:
        set_ftp_passwd(hosting.domain, passwd)
    return HttpResponseRedirect(reverse("phphost.dashboard.hosting", args=[hosting.domain, ])+"?alert=success")

@login_required
def set_mysql_passwd_process(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: passwd = request.POST["passwd"]
    except KeyError: passwd = None
    if passwd:
        set_mysql_passwd(hosting.domain, passwd)
    return HttpResponseRedirect(reverse("phphost.dashboard.hosting", args=[hosting.domain, ])+"?alert=success")