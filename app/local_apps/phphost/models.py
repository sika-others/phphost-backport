# coding=utf-8

from django.db import models
from django.contrib.auth.models import User

import datetime

import libssm

HOSTING_TYPES = (
    ("basic", "Basic (299Kč/rok)"),
    ("standard", "Standard (399Kč/rok)"),
    ("pro", "Pro (499Kč/rok)"),
    ("unlimit", "Unlimit (899Kč/rok)"),
)

class UserProfile(User):
    pass

class Hosting(models.Model):
    owner = models.ForeignKey(UserProfile)

    domain = models.CharField(max_length=255, unique=True)
    hosting_type = models.CharField(max_length=32, choices=HOSTING_TYPES)
    created_at = models.DateField(auto_now_add=True)
    last_payment = models.DateField(auto_now_add=True)

    def get_ssmdomain(self):
        return libssm.Domain(self.domain)

    def get_ftp_server(self):
        return "ftp.%s" % self.domain

    def get_ftp_user(self):
        return self.get_ssmdomain().user

    def get_mysql_server(self):
        return "localhost"

    def get_mysql_user(self):
        return self.get_ssmdomain().mysql_user

    def get_expire_date(self):
        return self.last_payment + datetime.timedelta(days=364)

    def __unicode__(self):
        return u"%s" % self.domain