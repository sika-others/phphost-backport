from django.views.generic.simple import redirect_to

urlpatterns += patterns("",
    url(r'^accounts/', include("registration.urls")),
    url(r'^accounts/profile/', redirect_to, {"url": "/dashboard"}),
    url(r'^register/', redirect_to, {"url": "/accounts/register/"}),
    url(r'^login/', redirect_to, {"url": "/accounts/login/"}),
)